# MYJ2C-Android

目前只支持依赖库混淆，不支持apk加固，需要minSdkVersion>=26

1. 测试混淆应用 MyComic安卓在线漫画、小说、番剧阅读器。源码地址https://gitee.com/luqichuang/MyComic
2.  MyComic-v1.5.0.55.apk 为原版未混淆apk
3.  MyComic-v1.5.0.55-myj2c.apk 为经过myj2c混淆过的apk，只混淆了common中的代码

源码
```
package top.luqichuang.myvideo.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.myvideo.model.BaseVideoSource;
import top.luqichuang.myvideo.model.VideoInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/24 18:33
 * @ver 1.0
 */
@Deprecated
public class FengChe extends BaseVideoSource {
    @Override
    public VSourceEnum getVSourceEnum() {
        return VSourceEnum.FENG_CHE;
    }

    @Override
    public String getIndex() {
        return "http://www.fengchedm.com";
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = getIndex() + "/common/search.aspx?key=%s";
        return NetUtil.getRequest(String.format(url, searchString));
    }

    @Override
    public List<VideoInfo> getInfoList(String html) {
        JsoupStarter<VideoInfo> starter = new JsoupStarter<VideoInfo>() {
            @Override
            protected VideoInfo dealElement(JsoupNode node) {
                String title = node.ownText("p a");
                String author = null;
                String updateTime = null;
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("a");
                return new VideoInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime);
            }
        };
        return starter.startElements(html, "div.imgs li");
    }

    @Override
    public void setInfoDetail(VideoInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("span.names");
                String imgUrl = node.src("div.tpic.l img");
                String author = null;
                String intro = node.ownText("div.info");
                String updateStatus = null;
                String updateTime = node.ownText("div.alex span:eq(3) a");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "div.movurl li"));
        SourceHelper.initChapterInfoMap(info, html, "div.tabs li strong:eq(0),div.movurl ul", false);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String url = null;
        try {
            String playType = "";
            String source = StringUtil.match("var source=\"(.*?)\"", html);
            String key = "";
            if ("".equals(source)) {
                playType = StringUtil.match("var playtype=\"(.*?)\"", html);
                key = StringUtil.match("id_(.*?).html", html);
            } else {
                String s = StringUtil.match("var dm456 = \"(.*?)\"", html);
                String[] ss = s.split("###");
                playType = ss[1];
                key = ss[3];
            }
            if ("iask".equals(playType)) {
                url = "http://ask.ivideo.sina.com.cn/v_play_ipad.php?vid=" + key + "&uid=1&pid=1&tid=334&plid=4001&prid=ja_7_2184731619&referrer=http%3A%2F%2Fvideo.sina.com.cn&ran=493&r=video.sina.com.cn&v=4.1.43.10&p=i&k=58e";
            } else if ("youku".equals(playType)) {
                url = "http://player.youku.com/embed/" + key;
            } else {
                url = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Content content = new Content(chapterId);
        content.setUrl(url);
        return SourceHelper.getContentList(content);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<div class=\"area box\">\n" +
                "<div class=\"box1 l\">\n" +
                "<label>按地区/时间</label><ul><li><a href=\"/type1/137-0-0-0-1.html\">国产</a></li><li><a href=\"/type1/138-0-0-0-1.html\">日本</a></li><li><a href=\"/type1/139-0-0-0-1.html\">欧美</a></li><li><a href=\"/type1/140-0-0-0-1.html\">韩国</a></li></ul><ul><li><a href=\"/type1/0-0-1-0-1.html\">2020</a></li><li><a href=\"/type1/0-0-2-0-1.html\">2019</a></li><li><a href=\"/type1/0-0-3-0-1.html\">2018</a></li><li><a href=\"/type1/0-0-4-0-1.html\">2017</a></li></ul></div>\n" +
                "<div class=\"box2 l\">\n" +
                "<label>按类型</label><ul><li><a href=\"/type1/0-1-0-0-1.html\">热血</a></li><li><a href=\"/type1/0-2-0-0-1.html\">格斗</a></li><li><a href=\"/type1/0-3-0-0-1.html\">恋爱</a></li><li><a href=\"/type1/0-4-0-0-1.html\">校园</a></li><li><a href=\"/type1/0-5-0-0-1.html\">搞笑</a></li><li><a href=\"/type1/0-6-0-0-1.html\">LOLI</a></li><li><a href=\"/type1/0-7-0-0-1.html\">神魔</a></li><li><a href=\"/type1/0-8-0-0-1.html\">机战</a></li><li><a href=\"/type1/0-9-0-0-1.html\">科幻</a></li><li><a href=\"/type1/0-10-0-0-1.html\">真人</a></li><li><a href=\"/type1/0-11-0-0-1.html\">青春</a></li><li><a href=\"/type1/0-12-0-0-1.html\">魔法</a></li><li><a href=\"/type1/0-13-0-0-1.html\">美少女</a></li><li><a href=\"/type1/0-14-0-0-1.html\">神话</a></li><li><a href=\"/type1/0-15-0-0-1.html\">冒险</a></li><li><a href=\"/type1/0-16-0-0-1.html\">运动</a></li><li><a href=\"/type1/0-17-0-0-1.html\">竞技</a></li><li><a href=\"/type1/0-18-0-0-1.html\">童话</a></li><li><a href=\"/type1/0-19-0-0-1.html\">亲子</a></li><li><a href=\"/type1/0-20-0-0-1.html\">教育</a></li><li><a href=\"/type1/0-21-0-0-1.html\">励志</a></li><li><a href=\"/type1/0-22-0-0-1.html\">剧情</a></li><li><a href=\"/type1/0-23-0-0-1.html\">社会</a></li><li><a href=\"/type1/0-24-0-0-1.html\">后宫</a></li><li><a href=\"/type1/0-25-0-0-1.html\">战争</a></li><li><a href=\"/type1/0-26-0-0-1.html\">吸血鬼</a></li></ul></div>\n" +
                "<div class=\"box3 r\">\n" +
                "<label>按语言</label><ul>\n" +
                "<li><a href=\"/type2/0-1-0-0-1.html\">日语</a></li><li><a href=\"/type2/0-2-0-0-1.html\">国语</a></li><li><a href=\"/type2/0-3-0-0-1.html\">粤语</a></li><li><a href=\"/type2/0-4-0-0-1.html\">英语</a></li><li><a href=\"/type2/0-5-0-0-1.html\">韩语</a></li><li><a href=\"/type2/0-6-0-0-1.html\">方言</a></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<VideoInfo> getRankInfoList(String html) {
        JsoupStarter<VideoInfo> starter = new JsoupStarter<VideoInfo>() {
            @Override
            protected VideoInfo dealElement(JsoupNode node) {
                String title = node.ownText("h2 a");
                String author = null;
                String updateTime = node.ownText("font");
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("a");
                return new VideoInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime);
            }
        };
        return starter.startElements(html, "div.pics li");
    }
}

```


未混淆反编译效果
```
/*
 * Decompiled with CFR 0.152.
 */
package top.luqichuang.myvideo.source;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okhttp3.Request;
import org.jsoup.nodes.Element;
import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.myvideo.model.BaseVideoSource;
import top.luqichuang.myvideo.model.VideoInfo;

@Deprecated
public class FengChe
extends BaseVideoSource {
    /*
     * WARNING - void declaration
     * Enabled unnecessary exception pruning
     */
    @Override
    public List<Content> getContentList(String object, int n, Map<String, Object> object2) {
        void var1_12;
        void var2_14;
        Object object3;
        block8: {
            Object var4_16;
            block7: {
                var4_16 = null;
                if ("".equals(StringUtil.match("var source=\"(.*?)\"", (String)object))) {
                    object3 = StringUtil.match("var playtype=\"(.*?)\"", (String)object);
                    String string = StringUtil.match("id_(.*?).html", (String)object);
                    break block7;
                }
                String[] stringArray = StringUtil.match("var dm456 = \"(.*?)\"", (String)object).split("###");
                object3 = stringArray[1];
                String string = stringArray[3];
            }
            try {
                void var1_5;
                if ("iask".equals(object3)) {
                    object3 = new StringBuilder();
                    String string = ((StringBuilder)object3).append("http://ask.ivideo.sina.com.cn/v_play_ipad.php?vid=").append((String)var1_5).append("&uid=1&pid=1&tid=334&plid=4001&prid=ja_7_2184731619&referrer=http%3A%2F%2Fvideo.sina.com.cn&ran=493&r=video.sina.com.cn&v=4.1.43.10&p=i&k=58e").toString();
                    break block8;
                }
                if ("youku".equals(object3)) {
                    object3 = new StringBuilder();
                    String string = ((StringBuilder)object3).append("http://player.youku.com/embed/").append((String)var1_5).toString();
                } else {
                    Object var1_8 = null;
                }
            }
            catch (Exception exception) {
                exception.printStackTrace();
                Object var1_11 = var4_16;
            }
        }
        object3 = new Content((int)var2_14);
        ((Content)object3).setUrl((String)var1_12);
        return SourceHelper.getContentList((Content)object3);
    }

    @Override
    public String getIndex() {
        return "http://www.fengchedm.com";
    }

    @Override
    public List<VideoInfo> getInfoList(String string) {
        return new JsoupStarter<VideoInfo>(this){
            final FengChe this$0;
            {
                this.this$0 = fengChe;
            }

            @Override
            protected VideoInfo dealElement(JsoupNode object) {
                String string = ((JsoupNode)object).ownText("p a");
                String string2 = ((JsoupNode)object).src("img");
                object = this.this$0.getIndex() + ((JsoupNode)object).href("a");
                return new VideoInfo(this.this$0.getSourceId(), string, null, (String)object, string2, null);
            }
        }.startElements(string, "div.imgs li");
    }

    @Override
    public List<VideoInfo> getRankInfoList(String string) {
        return new JsoupStarter<VideoInfo>(this){
            final FengChe this$0;
            {
                this.this$0 = fengChe;
            }

            @Override
            protected VideoInfo dealElement(JsoupNode object) {
                String string = ((JsoupNode)object).ownText("h2 a");
                String string2 = ((JsoupNode)object).ownText("font");
                String string3 = ((JsoupNode)object).src("img");
                object = this.this$0.getIndex() + ((JsoupNode)object).href("a");
                return new VideoInfo(this.this$0.getSourceId(), string, null, (String)object, string3, string2);
            }
        }.startElements(string, "div.pics li");
    }

    @Override
    public Map<String, String> getRankMap() {
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<String, String>();
        JsoupNode jsoupNode = new JsoupNode("<div class=\"area box\">\n<div class=\"box1 l\">\n<label>\u6309\u5730\u533a/\u65f6\u95f4</label><ul><li><a href=\"/type1/137-0-0-0-1.html\">\u56fd\u4ea7</a></li><li><a href=\"/type1/138-0-0-0-1.html\">\u65e5\u672c</a></li><li><a href=\"/type1/139-0-0-0-1.html\">\u6b27\u7f8e</a></li><li><a href=\"/type1/140-0-0-0-1.html\">\u97e9\u56fd</a></li></ul><ul><li><a href=\"/type1/0-0-1-0-1.html\">2020</a></li><li><a href=\"/type1/0-0-2-0-1.html\">2019</a></li><li><a href=\"/type1/0-0-3-0-1.html\">2018</a></li><li><a href=\"/type1/0-0-4-0-1.html\">2017</a></li></ul></div>\n<div class=\"box2 l\">\n<label>\u6309\u7c7b\u578b</label><ul><li><a href=\"/type1/0-1-0-0-1.html\">\u70ed\u8840</a></li><li><a href=\"/type1/0-2-0-0-1.html\">\u683c\u6597</a></li><li><a href=\"/type1/0-3-0-0-1.html\">\u604b\u7231</a></li><li><a href=\"/type1/0-4-0-0-1.html\">\u6821\u56ed</a></li><li><a href=\"/type1/0-5-0-0-1.html\">\u641e\u7b11</a></li><li><a href=\"/type1/0-6-0-0-1.html\">LOLI</a></li><li><a href=\"/type1/0-7-0-0-1.html\">\u795e\u9b54</a></li><li><a href=\"/type1/0-8-0-0-1.html\">\u673a\u6218</a></li><li><a href=\"/type1/0-9-0-0-1.html\">\u79d1\u5e7b</a></li><li><a href=\"/type1/0-10-0-0-1.html\">\u771f\u4eba</a></li><li><a href=\"/type1/0-11-0-0-1.html\">\u9752\u6625</a></li><li><a href=\"/type1/0-12-0-0-1.html\">\u9b54\u6cd5</a></li><li><a href=\"/type1/0-13-0-0-1.html\">\u7f8e\u5c11\u5973</a></li><li><a href=\"/type1/0-14-0-0-1.html\">\u795e\u8bdd</a></li><li><a href=\"/type1/0-15-0-0-1.html\">\u5192\u9669</a></li><li><a href=\"/type1/0-16-0-0-1.html\">\u8fd0\u52a8</a></li><li><a href=\"/type1/0-17-0-0-1.html\">\u7ade\u6280</a></li><li><a href=\"/type1/0-18-0-0-1.html\">\u7ae5\u8bdd</a></li><li><a href=\"/type1/0-19-0-0-1.html\">\u4eb2\u5b50</a></li><li><a href=\"/type1/0-20-0-0-1.html\">\u6559\u80b2</a></li><li><a href=\"/type1/0-21-0-0-1.html\">\u52b1\u5fd7</a></li><li><a href=\"/type1/0-22-0-0-1.html\">\u5267\u60c5</a></li><li><a href=\"/type1/0-23-0-0-1.html\">\u793e\u4f1a</a></li><li><a href=\"/type1/0-24-0-0-1.html\">\u540e\u5bab</a></li><li><a href=\"/type1/0-25-0-0-1.html\">\u6218\u4e89</a></li><li><a href=\"/type1/0-26-0-0-1.html\">\u5438\u8840\u9b3c</a></li></ul></div>\n<div class=\"box3 r\">\n<label>\u6309\u8bed\u8a00</label><ul>\n<li><a href=\"/type2/0-1-0-0-1.html\">\u65e5\u8bed</a></li><li><a href=\"/type2/0-2-0-0-1.html\">\u56fd\u8bed</a></li><li><a href=\"/type2/0-3-0-0-1.html\">\u7ca4\u8bed</a></li><li><a href=\"/type2/0-4-0-0-1.html\">\u82f1\u8bed</a></li><li><a href=\"/type2/0-5-0-0-1.html\">\u97e9\u8bed</a></li><li><a href=\"/type2/0-6-0-0-1.html\">\u65b9\u8a00</a></li>\n</ul>\n</div>\n</div>");
        Iterator iterator = jsoupNode.getElements("a").iterator();
        while (iterator.hasNext()) {
            jsoupNode.init((Element)iterator.next());
            linkedHashMap.put(jsoupNode.ownText("a"), this.getIndex() + jsoupNode.href("a"));
        }
        return linkedHashMap;
    }

    @Override
    public Request getSearchRequest(String string) {
        return NetUtil.getRequest(String.format(this.getIndex() + "/common/search.aspx?key=%s", string));
    }

    @Override
    public VSourceEnum getVSourceEnum() {
        return VSourceEnum.FENG_CHE;
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public void setInfoDetail(VideoInfo videoInfo, String string, Map<String, Object> object) {
        object = new JsoupStarter<ChapterInfo>(this, videoInfo){
            final FengChe this$0;
            final VideoInfo val$info;
            {
                this.this$0 = fengChe;
                this.val$info = videoInfo;
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode jsoupNode) {
                return new ChapterInfo(jsoupNode.ownText("a"), this.this$0.getIndex() + jsoupNode.href("a"));
            }

            @Override
            protected void dealInfo(JsoupNode object) {
                String string = ((JsoupNode)object).ownText("span.names");
                String string2 = ((JsoupNode)object).src("div.tpic.l img");
                String string3 = ((JsoupNode)object).ownText("div.info");
                object = ((JsoupNode)object).ownText("div.alex span:eq(3) a");
                this.val$info.setDetail(string, string2, null, (String)object, null, string3);
            }

            @Override
            protected boolean isDESC() {
                return false;
            }
        };
        ((JsoupStarter)object).startInfo(string);
        SourceHelper.initChapterInfoList(videoInfo, ((JsoupStarter)object).startElements(string, "div.movurl li"));
        SourceHelper.initChapterInfoMap(videoInfo, string, "div.tabs li strong:eq(0),div.movurl ul", false);
    }
}

```

混淆后反编译
```
/*
 * Decompiled with CFR 0.152.
 */
package top.luqichuang.myvideo.source;

import java.lang.invoke.CallSite;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import myj2c.rfDHjU.Myj2cLoader;
import okhttp3.Request;
import org.jsoup.nodes.Element;
import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.myvideo.model.BaseVideoSource;
import top.luqichuang.myvideo.model.VideoInfo;

@Deprecated
public class FengChe
extends BaseVideoSource {
    private static final int[] IIIIllIlIl;
    private static Class[] lIIIlIIllI;
    private static String[] lIIlIllIII;
    private static final String[] lIllllIIll;

    private static native void $myj2cClinit();

    public static native void $myj2cLoader();

    static {
        Myj2cLoader.registerNativesForClass(14, FengChe.class);
        FengChe.$myj2cLoader();
        FengChe.$myj2cClinit();
    }

    private static native String THISISPROTECTEDBYMYJ2CIllllIIIIIcase(String var0, int var1);

    private static native String THISISPROTECTEDBYMYJ2ClIIlIlIllIcase(String var0, int var1);

    private static native String THISISPROTECTEDBYMYJ2CllIIlIIlIIcase(String var0, int var1);

    private static native void THISMETHODISPROTECTEDBYMYJ2CllIlllIlIlif();

    private static native CallSite importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C(MethodHandles.Lookup var0, String var1, MethodType var2) throws NoSuchMethodException, IllegalAccessException;

    private static native void shortIlIlIIIlIITHISISPROTECTEDBYMYJ2CJmrNrgE();

    private static native void strictfpSsxffnSBHxcakVvbTGoKczKThZIVycFJNqMXxVUQkETHISISPROTECTEDBYMYJ2C();

    /*
     * Enabled unnecessary exception pruning
     */
    @Override
    public List<Content> getContentList(String object, int n, Map<String, Object> object2) {
        Object object3;
        block10: {
            int[] nArray;
            String[] stringArray;
            CallSite callSite;
            block9: {
                callSite = null;
                stringArray = lIllllIIll;
                nArray = IIIIllIlIl;
                object2 = stringArray[nArray[5]];
                object3 = FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("8", stringArray[nArray[6]], (String)object);
                object2 = stringArray[nArray[7]];
                if (stringArray[nArray[8]].equals(object3)) {
                    object2 = FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("9", stringArray[nArray[9]], (String)object);
                    object = FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("10", stringArray[nArray[10]], (String)object);
                    break block9;
                }
                object = ((String)((Object)FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("11", stringArray[nArray[11]], (String)object))).split(stringArray[nArray[12]]);
                object2 = object[nArray[1]];
                object = object[nArray[3]];
            }
            try {
                if (stringArray[nArray[13]].equals(object2)) {
                    object2 = new StringBuilder();
                    object = ((StringBuilder)object2).append(stringArray[nArray[14]]).append((String)object).append(stringArray[nArray[15]]);
                } else {
                    object3 = callSite;
                    if (!stringArray[nArray[16]].equals(object2)) break block10;
                    object2 = new StringBuilder();
                    object = ((StringBuilder)object2).append(stringArray[nArray[17]]).append((String)object);
                }
                object3 = ((StringBuilder)object).toString();
            }
            catch (Exception exception) {
                exception.printStackTrace();
                object3 = callSite;
            }
        }
        object = new Content(n);
        FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("12", (Content)object, object3);
        return FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("13", (Content)object);
    }

    @Override
    public String getIndex() {
        return lIllllIIll[IIIIllIlIl[0]];
    }

    @Override
    public List<VideoInfo> getInfoList(String string) {
        JsoupStarter<VideoInfo> jsoupStarter = new JsoupStarter<VideoInfo>(this){
            private static final int[] IlIIIlIIIl;
            private static final String[] IlIIllIlIl;
            private static Class[] IlllIIlIlI;
            private static String[] IlllIlIIlI;
            final FengChe this$0;

            private static native void $myj2cClinit();

            public static native void $myj2cLoader();

            static {
                Myj2cLoader.registerNativesForClass(66, 1.class);
                1.$myj2cLoader();
                1.$myj2cClinit();
            }
            {
                this.this$0 = fengChe;
            }

            private static native String THISISPROTECTEDBYMYJ2CIIIIIlIIlIlong(String var0, int var1);

            private static native String THISISPROTECTEDBYMYJ2CIlIIIllIIldefault(String var0, int var1);

            private static native void THISMETHODISPROTECTEDBYMYJ2CIlIlIllIIltransient();

            private static native void finalIIllIlIIIITHISISPROTECTEDBYMYJ2CQWrXemns();

            private static native void importCDYHUsWLRkYtHnZHqMxEIWoTHISISPROTECTEDBYMYJ2C();

            private static native CallSite privatelIIIIIIIIITHISMETHODISPROTECTEDBYMYJ2C(MethodHandles.Lookup var0, String var1, MethodType var2) throws NoSuchMethodException, IllegalAccessException;

            @Override
            protected VideoInfo dealElement(JsoupNode object) {
                String[] stringArray = IlIIllIlIl;
                int[] nArray = IlIIIlIIIl;
                CallSite callSite = 1.privatelIIIIIIIIITHISMETHODISPROTECTEDBYMYJ2C("0", (JsoupNode)object, (String)stringArray[nArray[0]]);
                CallSite callSite2 = 1.privatelIIIIIIIIITHISMETHODISPROTECTEDBYMYJ2C("1", (JsoupNode)object, (String)stringArray[nArray[1]]);
                object = (String)((Object)1.privatelIIIIIIIIITHISMETHODISPROTECTEDBYMYJ2C("3", (FengChe)((Object)1.privatelIIIIIIIIITHISMETHODISPROTECTEDBYMYJ2C("2", this)))) + (String)((Object)1.privatelIIIIIIIIITHISMETHODISPROTECTEDBYMYJ2C("4", (JsoupNode)object, (String)stringArray[nArray[2]]));
                return new VideoInfo((int)1.privatelIIIIIIIIITHISMETHODISPROTECTEDBYMYJ2C("6", (FengChe)((Object)1.privatelIIIIIIIIITHISMETHODISPROTECTEDBYMYJ2C("5", this))), (String)((Object)callSite), null, (String)object, (String)((Object)callSite2), null);
            }
        };
        int[] nArray = IIIIllIlIl;
        String[] stringArray = new String[nArray[1]];
        stringArray[nArray[0]] = lIllllIIll[nArray[2]];
        return FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("3", (JsoupStarter)jsoupStarter, (String)string, (String[])stringArray);
    }

    @Override
    public List<VideoInfo> getRankInfoList(String string) {
        JsoupStarter<VideoInfo> jsoupStarter = new JsoupStarter<VideoInfo>(this){
            private static String[] IIllIIIIIl;
            private static final int[] IlllllIllI;
            private static Class[] llIIlIlIIl;
            private static final String[] llIlllIIII;
            final FengChe this$0;

            private static native void $myj2cClinit();

            public static native void $myj2cLoader();

            static {
                Myj2cLoader.registerNativesForClass(59, 3.class);
                3.$myj2cLoader();
                3.$myj2cClinit();
            }
            {
                this.this$0 = fengChe;
            }

            private static native String THISISPROTECTEDBYMYJ2CIIlllIllllreturn(String var0, int var1);

            private static native String THISISPROTECTEDBYMYJ2CIlIlIIIIlIimplements(String var0, int var1);

            private static native void THISMETHODISPROTECTEDBYMYJ2CIIIlllllllprotected();

            private static native void doIIlllIIIlITHISISPROTECTEDBYMYJ2CKjkLLrFWKH();

            private static native void instanceofJlpBOnxjrLalXfKxBPjmPMXaTHISISPROTECTEDBYMYJ2C();

            private static native CallSite thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C(MethodHandles.Lookup var0, String var1, MethodType var2) throws NoSuchMethodException, IllegalAccessException;

            @Override
            protected VideoInfo dealElement(JsoupNode object) {
                String[] stringArray = llIlllIIII;
                int[] nArray = IlllllIllI;
                CallSite callSite = 3.thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C("0", (JsoupNode)object, (String)stringArray[nArray[0]]);
                CallSite callSite2 = 3.thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C("1", (JsoupNode)object, (String)stringArray[nArray[1]]);
                CallSite callSite3 = 3.thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C("2", (JsoupNode)object, (String)stringArray[nArray[2]]);
                object = (String)((Object)3.thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C("4", (FengChe)((Object)3.thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C("3", this)))) + (String)((Object)3.thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C("5", (JsoupNode)object, (String)stringArray[nArray[3]]));
                return new VideoInfo((int)3.thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C("7", (FengChe)((Object)3.thislIlIlIIlllTHISMETHODISPROTECTEDBYMYJ2C("6", this))), (String)((Object)callSite), null, (String)object, (String)((Object)callSite3), (String)((Object)callSite2));
            }
        };
        int[] nArray = IIIIllIlIl;
        String[] stringArray = new String[nArray[1]];
        stringArray[nArray[0]] = lIllllIIll[nArray[21]];
        return FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("19", (JsoupStarter)jsoupStarter, (String)string, (String[])stringArray);
    }

    @Override
    public Map<String, String> getRankMap() {
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<String, String>();
        JsoupNode jsoupNode = new JsoupNode("<div class=\"area box\">\n<div class=\"box1 l\">\n<label>\u6309\u5730\u533a/\u65f6\u95f4</label><ul><li><a href=\"/type1/137-0-0-0-1.html\">\u56fd\u4ea7</a></li><li><a href=\"/type1/138-0-0-0-1.html\">\u65e5\u672c</a></li><li><a href=\"/type1/139-0-0-0-1.html\">\u6b27\u7f8e</a></li><li><a href=\"/type1/140-0-0-0-1.html\">\u97e9\u56fd</a></li></ul><ul><li><a href=\"/type1/0-0-1-0-1.html\">2020</a></li><li><a href=\"/type1/0-0-2-0-1.html\">2019</a></li><li><a href=\"/type1/0-0-3-0-1.html\">2018</a></li><li><a href=\"/type1/0-0-4-0-1.html\">2017</a></li></ul></div>\n<div class=\"box2 l\">\n<label>\u6309\u7c7b\u578b</label><ul><li><a href=\"/type1/0-1-0-0-1.html\">\u70ed\u8840</a></li><li><a href=\"/type1/0-2-0-0-1.html\">\u683c\u6597</a></li><li><a href=\"/type1/0-3-0-0-1.html\">\u604b\u7231</a></li><li><a href=\"/type1/0-4-0-0-1.html\">\u6821\u56ed</a></li><li><a href=\"/type1/0-5-0-0-1.html\">\u641e\u7b11</a></li><li><a href=\"/type1/0-6-0-0-1.html\">LOLI</a></li><li><a href=\"/type1/0-7-0-0-1.html\">\u795e\u9b54</a></li><li><a href=\"/type1/0-8-0-0-1.html\">\u673a\u6218</a></li><li><a href=\"/type1/0-9-0-0-1.html\">\u79d1\u5e7b</a></li><li><a href=\"/type1/0-10-0-0-1.html\">\u771f\u4eba</a></li><li><a href=\"/type1/0-11-0-0-1.html\">\u9752\u6625</a></li><li><a href=\"/type1/0-12-0-0-1.html\">\u9b54\u6cd5</a></li><li><a href=\"/type1/0-13-0-0-1.html\">\u7f8e\u5c11\u5973</a></li><li><a href=\"/type1/0-14-0-0-1.html\">\u795e\u8bdd</a></li><li><a href=\"/type1/0-15-0-0-1.html\">\u5192\u9669</a></li><li><a href=\"/type1/0-16-0-0-1.html\">\u8fd0\u52a8</a></li><li><a href=\"/type1/0-17-0-0-1.html\">\u7ade\u6280</a></li><li><a href=\"/type1/0-18-0-0-1.html\">\u7ae5\u8bdd</a></li><li><a href=\"/type1/0-19-0-0-1.html\">\u4eb2\u5b50</a></li><li><a href=\"/type1/0-20-0-0-1.html\">\u6559\u80b2</a></li><li><a href=\"/type1/0-21-0-0-1.html\">\u52b1\u5fd7</a></li><li><a href=\"/type1/0-22-0-0-1.html\">\u5267\u60c5</a></li><li><a href=\"/type1/0-23-0-0-1.html\">\u793e\u4f1a</a></li><li><a href=\"/type1/0-24-0-0-1.html\">\u540e\u5bab</a></li><li><a href=\"/type1/0-25-0-0-1.html\">\u6218\u4e89</a></li><li><a href=\"/type1/0-26-0-0-1.html\">\u5438\u8840\u9b3c</a></li></ul></div>\n<div class=\"box3 r\">\n<label>\u6309\u8bed\u8a00</label><ul>\n<li><a href=\"/type2/0-1-0-0-1.html\">\u65e5\u8bed</a></li><li><a href=\"/type2/0-2-0-0-1.html\">\u56fd\u8bed</a></li><li><a href=\"/type2/0-3-0-0-1.html\">\u7ca4\u8bed</a></li><li><a href=\"/type2/0-4-0-0-1.html\">\u82f1\u8bed</a></li><li><a href=\"/type2/0-5-0-0-1.html\">\u97e9\u8bed</a></li><li><a href=\"/type2/0-6-0-0-1.html\">\u65b9\u8a00</a></li>\n</ul>\n</div>\n</div>");
        Iterator iterator = ((ArrayList)((Object)FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("14", (JsoupNode)jsoupNode, (String)lIllllIIll[IIIIllIlIl[18]]))).iterator();
        while (iterator.hasNext()) {
            FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("15", (JsoupNode)jsoupNode, (Element)((Element)iterator.next()));
            String[] stringArray = lIllllIIll;
            int[] nArray = IIIIllIlIl;
            linkedHashMap.put((String)((Object)FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("16", (JsoupNode)jsoupNode, (String)stringArray[nArray[19]])), (String)((Object)FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("17", (FengChe)this)) + (String)((Object)FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("18", (JsoupNode)jsoupNode, (String)stringArray[nArray[20]])));
        }
        return linkedHashMap;
    }

    @Override
    public Request getSearchRequest(String string) {
        CharSequence charSequence = new StringBuilder().append((String)((Object)FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("1", (FengChe)this)));
        Object[] objectArray = lIllllIIll;
        int[] nArray = IIIIllIlIl;
        charSequence = charSequence.append(objectArray[nArray[1]]).toString();
        objectArray = new Object[nArray[1]];
        objectArray[nArray[0]] = string;
        return FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("2", String.format((String)charSequence, objectArray));
    }

    @Override
    public VSourceEnum getVSourceEnum() {
        return FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("0");
    }

    @Override
    public boolean isValid() {
        return IIIIllIlIl[0];
    }

    @Override
    public void setInfoDetail(VideoInfo videoInfo, String string, Map<String, Object> object) {
        JsoupStarter<ChapterInfo> jsoupStarter = new JsoupStarter<ChapterInfo>(this, videoInfo){
            private static final int[] IIIIIIlIlI;
            private static String[] IlllllIlII;
            private static Class[] lIIllIlIIl;
            final FengChe this$0;
            final VideoInfo val$info;

            private static native void $myj2cClinit();

            public static native void $myj2cLoader();

            static {
                Myj2cLoader.registerNativesForClass(55, 2.class);
                2.$myj2cLoader();
                2.$myj2cClinit();
            }
            {
                this.this$0 = fengChe;
                this.val$info = videoInfo;
            }

            private static native String THISISPROTECTEDBYMYJ2CIIIIIllIlIshort(String var0, int var1);

            private static native String THISISPROTECTEDBYMYJ2ClIIlllIlIIsuper(String var0, int var1);

            private static native String THISISPROTECTEDBYMYJ2ClIlIIIllllswitch(String var0, int var1);

            private static native void THISMETHODISPROTECTEDBYMYJ2CllIIIllIllstrictfp();

            private static native void continuevPcIlJUzmKkeUaJogywmsToUGPZQZVbXFvSnUBuTHISISPROTECTEDBYMYJ2C();

            private static native void interfaceIllIIIllIITHISISPROTECTEDBYMYJ2ClqHAz();

            private static native CallSite transientIIIlllIllITHISMETHODISPROTECTEDBYMYJ2C(MethodHandles.Lookup var0, String var1, MethodType var2) throws NoSuchMethodException, IllegalAccessException;

            @Override
            protected ChapterInfo dealElement(JsoupNode jsoupNode) {
                String[] stringArray = IlllllIlII;
                int[] nArray = IIIIIIlIlI;
                return new ChapterInfo((String)((Object)2.transientIIIlllIllITHISMETHODISPROTECTEDBYMYJ2C("6", (JsoupNode)jsoupNode, (String)stringArray[nArray[4]])), (String)((Object)2.transientIIIlllIllITHISMETHODISPROTECTEDBYMYJ2C("8", (FengChe)((Object)2.transientIIIlllIllITHISMETHODISPROTECTEDBYMYJ2C("7", this)))) + (String)((Object)2.transientIIIlllIllITHISMETHODISPROTECTEDBYMYJ2C("9", (JsoupNode)jsoupNode, (String)IlllllIlII[nArray[5]])));
            }

            @Override
            protected void dealInfo(JsoupNode var1);
        };
        FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("4", (JsoupStarter)jsoupStarter, (String)string);
        object = IIIIllIlIl;
        String[] stringArray = new String[object[1]];
        Object object2 = object[0];
        String[] stringArray2 = lIllllIIll;
        stringArray[object2] = stringArray2[object[3]];
        FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("6", (EntityInfo)videoInfo, (List)((Object)FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("5", (JsoupStarter)jsoupStarter, (String)string, (String[])stringArray)));
        FengChe.importIllllIllIITHISMETHODISPROTECTEDBYMYJ2C("7", (EntityInfo)videoInfo, (String)string, (String)stringArray2[object[4]], (boolean)object[0]);
    }
}

```
